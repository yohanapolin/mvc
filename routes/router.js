const express = require("express");
const router = express.Router();

const controller = require("../controllers/controllers");

var checkAuthentication = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    } else {
      res.redirect("/login");
    }
  };

router.get("/showUsers", controller.showUsers);

router.get("/", controller.getHomePage);
router.get("/register", controller.getRegisterPage);
router.get("/login", controller.getLoginPage);
router.get("/dashboard", checkAuthentication, controller.getDashboardPage);
router.get("/logout", controller.logout);

router.post("/register", controller.register);
router.post("/login", controller.login);

module.exports = router;