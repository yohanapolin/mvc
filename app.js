const express = require("express");
const path = require("path");
const passport = require("passport");
var LocalStrategy = require("passport-local");
var session = require("express-session");
const bcrypt = require("bcrypt");

const router = require("./routes/router");
const { User} = require("./models");
const app = express();

app.use(express.json());
app.use(express.urlencoded());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')))

//session express

app.use(
    session({
        secret: "secret",
        resave: true,
        saveUninitialized: true,
  })
);

//passport config

passport.use(
    new LocalStrategy(
      {
        usernameField: "username",
        passwordField: "password",
      },
      (email, password, done) => {
        console.log('masuk passport')
        User.findOne({
            where: {
             username: username,
            },
          })
          .then((User) => {
            console.log(User);
            if (User && User.dataValues) {
              const encryptedPassword = User.dataValues.password
    
              if (bcrypt.compareSync(password, encryptedPassword)) {
                return done(null, User)
              } else {
                return done(null, false)
              }
            } else {
              return done(null, false)
            }
          });
      }
    )
  );
  passport.serializeUser((User, done) => {
    done(null, User);
  });
  passport.deserializeUser((obj, done) => {
    done(null, obj);
  });
  
  // Passport middleware
  app.use(passport.initialize());
  app.use(passport.session());
  
  app.use("/", router);
  
  app.use((err, req, res, next) => {
    if (err) {
      console.log(err);
    }
  });
  
  app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`);
  });
  
  module.exports = app;
  