const { User } = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");

class Controller {
  static showUsers(req, res) {
    const usersArr = [];
    User.findAll({}).then((User) => {
      for (const users of User) {
        usersArr.push({
          email: users.dataValues.email,
          fName: users.dataValues.first_name,
          lName: users.dataValues.last_name,
        });
      }

      //   console.log(usersArr)
      res.render("show", {
        data: usersArr,
      });
    });
  }

  static register(req, res) {
    console.log(req.body)
    const email = req.body.email;
    const password = bcrypt.hashSync(req.body.password, 10);
    const username = req.body.username;
    const fullname = req.body.fullname;
    const avatar = req.body.avatar;

    User.create({
        email: email,
        password: password,
        username: username,
        fullname: fullname,
        avatar: avatar,
      })
      .then((users) => {
        console.log(users);
        res.render("login");
      })
      .catch((err) => {
        res.render("register");
      })
  }

  static login(req, res) {
    console.log("login controller");
    console.log(req.body)
    passport.authenticate("local", {
      successRedirect: "/dashboard", // GET
      failureRedirect: "/login", // GET
    })(req, res);
  }

  static getHomePage(req, res) {
    res.render("index");
  }

  static getRegisterPage(req, res) {
    res.render("register");
  }

  static getLoginPage(req, res) {
    res.render("login");
  }
  
  static getDashboardPage(req, res) {
    res.render("dashboard");
  }

  static logout(req, res) {
    req.logout();
    res.redirect("/");
  }
}

module.exports = Controller;
